from uuid import uuid4

import py2neo
from fastapi import HTTPException, status
from pydantic import parse_obj_as

from paperback.docs import schemas


def create_dict(
    tx: py2neo.Transaction,
    dictionary: schemas.DictionaryCreate,
) -> schemas.Dictionary:
    dict_node = py2neo.Node(
        "Dictionary",
        name=dictionary.name,
    )
    if tx.exists(dict_node):
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="Dictionary with this name exists",
        )

    dict_node["words"] = dictionary.words
    dict_node["dict_uuid"] = str(uuid4())

    tx.create(dict_node)
    tx.graph.commit(tx)

    return parse_obj_as(schemas.Dictionary, dict_node)


def get_dicts(
    tx: py2neo.Transaction,
) -> list[schemas.Dictionary]:
    dicts: list[py2neo.Node] = list(tx.graph.nodes.match("Dictionary"))
    return parse_obj_as(list[schemas.Dictionary], dicts)


def get_dict_node_by_name(
    tx: py2neo.Transaction,
    name: str,
) -> py2neo.Node:
    dictionary: py2neo.Node | None = tx.graph.nodes.match(
        "Dictionary", name=name
    ).first()
    if dictionary is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Dictionary with specified name wasn't found",
        )
    else:
        return dictionary


def get_dict_by_name(
    tx: py2neo.Transaction,
    name: str,
) -> schemas.DocOut:
    dictionary: py2neo.Node = get_dict_node_by_name(tx, name)
    return parse_obj_as(schemas.Dictionary, dictionary)


def add_word_to_dict_by_name(
    tx: py2neo.Transaction,
    name: str,
    word: str,
) -> schemas.Dictionary:
    dictionary: py2neo.Node = get_dict_node_by_name(tx, name)
    if word in dictionary["words"]:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="Dictionary already has specified word",
        )
    dictionary["words"] += word
    tx.push(dictionary)
    tx.graph.commit(tx)
    return parse_obj_as(schemas.Dictionary, dictionary)


def delete_word_from_dict_by_name(
    tx: py2neo.Transaction,
    name: str,
    word: str,
) -> schemas.Dictionary:
    dictionary: py2neo.Node = get_dict_node_by_name(tx, name)
    if word not in dictionary["words"]:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="Document doesn't have specified word",
        )
    dictionary["words"] = list(dictionary["words"]).remove(word)
    tx.push(dictionary)
    tx.graph.commit(tx)
    return parse_obj_as(schemas.Dictionary, dictionary)


def delete_dict_by_name(
    tx: py2neo.Transaction,
    name: str,
):
    dictionary: py2neo.Node = get_dict_by_name(tx, name)
    tx.delete(dictionary)
    tx.graph.commit(tx)
