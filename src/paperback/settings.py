from functools import lru_cache
from pathlib import Path

from pydantic import BaseSettings


class AppSettings(BaseSettings):
    log_level: str = "INFO"
    config_dir: Path = Path.home() / ".papertext"


@lru_cache()
def get_settings():
    """
    FastAPI dependency
    """
    return AppSettings()
