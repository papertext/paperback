TODOS:
- [ ] make storage path consistent across modules\
    сделать глобальную настройку для пути и в модулях сделать опционально\
    и если не указано то брать рут и добавлять название модуля
- [x] rename analyzers to parsers to avoid confusion with analyze paths
- [x] fix lanch.json
- [x] fix poetry not working inside container
- [x] add tasks from poetry
- [x] add check on create that *ent* with specefied name doesn't exist
- [x] implement tag support
- [ ] task compression
- [ ] replace neo4j and psql links in docs settings with pydantic DSN object (and maybe in auth)
- [x] move parser parameters into worker container so they don't get exposed
- [x] add info to task status
- [x] don't rebuild containers in compose
- [x] add flower
- [x] add multiple workers support for production
- [x] add volumes for storing db (neo4j, psql, redis, rabitmq) data
- [ ] add networks to compose?
- [ ] move CI/CD to kubernetes?
- [x] add persistent storage for rabbitmq
