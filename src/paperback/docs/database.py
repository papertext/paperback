from typing import Iterator

from py2neo import Graph, Transaction
from py2neo.ogm import Repository

from paperback.docs.settings import get_docs_settings

settings = get_docs_settings()


repo = Repository(
    f"{settings.neo4j_scheme}://{settings.neo4j_user}:{settings.neo4j_pass}@{settings.neo4j_host}:{settings.neo4j_port}"
)


def get_transaction() -> Iterator[Transaction]:
    """
    FastAPI dependency
    """
    graph: Graph = repo.graph
    transaction = graph.begin()
    try:
        yield transaction
    finally:
        graph.rollback(transaction)
