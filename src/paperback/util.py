from typing import Literal, Type

from starlette.responses import JSONResponse


def get_async_lib_name() -> Literal["asyncio", "uvloop"]:
    try:
        import uvloop

        uvloop.install()

        return "uvloop"
    except ImportError:
        return "asyncio"


def get_response_class() -> Type[JSONResponse]:
    try:
        import orjson
        from fastapi.responses import ORJSONResponse

        return ORJSONResponse
    except ImportError:
        return JSONResponse
