from enum import Enum

from fastapi import Depends

from paperback.docs.parsers.abc import Parser, ParserResult
from paperback.docs.parsers.implementation import PyExLingWrapper, TitanisWrapper
from paperback.docs.settings import DocsSettings, get_docs_settings


class ParserEnum(str, Enum):
    pyexling = "pyexling"
    titanis_open = "titanis_open"


DEFAULT_PARSER: ParserEnum = ParserEnum.pyexling

ParserEnum2Wrapper: dict[ParserEnum, type[Parser]] = {
    ParserEnum.pyexling: PyExLingWrapper,
    ParserEnum.titanis_open: TitanisWrapper,
}


def get_parser_dict(
    settings: DocsSettings = Depends(get_docs_settings),
) -> dict[ParserEnum, Parser]:
    """
    FastAPI dependency
    """
    return {
        ParserEnum.pyexling: PyExLingWrapper(
            settings.parsers_pyexling_host,
            settings.parsers_pyexling_service,
            settings.parsers_titanis_host,
        ),
        ParserEnum.titanis_open: TitanisWrapper(
            settings.parsers_titanis_host,
        ),
    }
