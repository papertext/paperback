from functools import lru_cache
from pathlib import Path
from typing import Literal

from pydantic import BaseSettings, validator

from paperback.settings import get_settings

app_settings = get_settings()


class AuthSettings(BaseSettings):
    psql_scheme: str = "postgresql"
    psql_user: str = "postgres"
    psql_pass: str = "password"
    psql_host: str = "localhost"
    psql_port: str = "5432"
    psql_name: str = "auth_module"

    create_root_user: bool = False
    root_user_password: str | None = None

    @validator("root_user_password")
    def root_user_password_must_be_set_only_when_create_root_user_is_set(
        cls, root_user_password: str | None, values
    ):
        if (
            ("create_root_user" in values)
            and values["create_root_user"]
            and root_user_password is None
        ):
            raise ValueError(
                "root_user_password must be set when create_root_user is set to True"
            )
        if (
            ("create_root_user" in values)
            and not values["create_root_user"]
            and root_user_password is not None
        ):
            raise ValueError(
                "root_user_password must not be set when create_root_user is set to False"
            )
        return root_user_password

    storage_path: Path = app_settings.config_dir / "auth"

    recreate_keys: bool = False
    curve: str = "secp521r1"

    class Config:
        env_prefix = "auth__"

    @validator("storage_path")
    def storage_path_must_exist_and_be_a_folder(cls, storage_path: Path):
        if storage_path.exists():
            if not storage_path.is_dir():
                raise ValueError("storage path must be a dir")
        else:
            storage_path.mkdir(parents=True)
        return storage_path


@lru_cache()
def get_auth_settings():
    """
    FastAPI dependency
    """
    return AuthSettings()
