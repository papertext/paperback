from uuid import uuid4

import py2neo
from fastapi import HTTPException, status
from pydantic import parse_obj_as

from paperback.docs import schemas
from paperback.docs.crud.docs import get_doc_node_by_name
from paperback.docs.logging import logger


def create_corpus(
    tx: py2neo.Transaction,
    corp: schemas.CorpusCreate,
) -> schemas.Corpus:
    corp_node = py2neo.Node(
        "Corpus",
        name=corp.name,
    )
    if tx.exists(corp_node):
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail="Corpus with this name exists"
        )

    corp_node["corpus_uuid"] = str(uuid4())
    tx.create(corp_node)

    tx.graph.commit(tx)

    return parse_obj_as(schemas.Corpus, corp_node)


def get_corpuses(
    tx: py2neo.Transaction,
) -> list[schemas.Corpus]:
    """
    Note
    ----
    about cyphers (list comprehension)[https://neo4j.com/docs/cypher-manual/current/syntax/lists/#cypher-pattern-comprehension]
    list comprehension allows getting data as list by default, removing need to convert it later
    """
    corpuses = tx.run(
        """
    MATCH (corp:Corpus)
    RETURN
        corp.name AS name,
        corp.corpus_uuid as corpus_uuid,
        [(corp)-[:contains]->(sub_doc:Document) | sub_doc.name] AS includes_docs,
        [(corp)-[:contains]->(sub_corp:Corpus) | sub_corp.name] AS includes_corps;
    """
    ).data()
    return parse_obj_as(list[schemas.Corpus], corpuses)


def get_corpus_node_by_name(
    tx: py2neo.Transaction,
    name: str,
) -> py2neo.Node:
    corpus: py2neo.Node | None = tx.graph.nodes.match("Corpus", name=name).first()
    if corpus is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Corpus with specified name wasn't found",
        )
    else:
        return corpus


def get_corpus_by_name(
    tx: py2neo.Transaction,
    name: str,
) -> schemas.Corpus:
    """
    Note
    ----
    see `get_corpuses` for info on list comprehension
    """
    corpus = tx.run(
        """
    MATCH (corp:Corpus {name: $name})
    RETURN
        corp.name AS name,
        corp.corpus_uuid as corpus_uuid,
        [(corp)-[:contains]->(sub_doc:Document) | sub_doc.name] AS includes_docs,
        [(corp)-[:contains]->(sub_corp:Corpus) | sub_corp.name] AS includes_corps;
    """,
        name=name,
    ).data()
    match len(corpus):
        case 0:
            raise HTTPException(
                status_code=status.HTTP_409_CONFLICT,
                detail="Corpus with specified name wasn't found",
            )
        case 1:
            return parse_obj_as(schemas.Corpus, corpus)
        case _:
            raise HTTPException(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                detail="Multiple corpuses with the same name exist, contact administrator",
            )


def add_subcorpus_to_corpus_by_name(
    tx: py2neo.Transaction,
    name: str,
    sub_name: str,
):
    corpus: py2neo.Node = get_corpus_node_by_name(tx, name)
    sub_corpus: py2neo.Node = get_corpus_node_by_name(tx, sub_name)
    contains = py2neo.Relationship(corpus, "contains", sub_corpus)
    if tx.exists(contains):
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="Corpus already contains this sub corpus",
        )
    else:
        tx.create(contains)
        tx.graph.commit(tx)


def add_document_to_corpus_by_name(
    tx: py2neo.Transaction,
    name: str,
    sub_name: str,
):
    corpus: py2neo.Node = get_corpus_node_by_name(tx, name)
    sub_doc: py2neo.Node = get_doc_node_by_name(tx, sub_name)
    contains = py2neo.Relationship(corpus, "contains", sub_doc)
    if tx.exists(contains):
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="Corpus already contains this document",
        )
    else:
        tx.create(contains)
        tx.graph.commit(tx)


def remove_subcorpus_from_corpus_by_name(
    tx: py2neo.Transaction,
    name: str,
    sub_name: str,
):
    corpus: py2neo.Node = get_corpus_node_by_name(tx, name)
    sub_corpus: py2neo.Node = get_corpus_node_by_name(tx, sub_name)
    contains = tx.graph.relationships.match((corpus, sub_corpus), "contains")
    if contains.exists():
        tx.delete(contains.all())
        tx.graph.commit(tx)
    else:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Specefied corpus doesn't contain specedied sub-corpus",
        )


def remove_document_from_corpus_by_name(
    tx: py2neo.Transaction,
    name: str,
    sub_name: str,
):
    corpus: py2neo.Node = get_corpus_node_by_name(tx, name)
    sub_doc: py2neo.Node = get_doc_node_by_name(tx, sub_name)
    contains = tx.graph.relationships.match((corpus, sub_doc), "contains")
    if contains.exists():
        tx.delete(contains.all())
        tx.graph.commit(tx)
    else:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Specefied corpus doesn't contain specedied document",
        )


def delete_corpus_by_name(
    tx: py2neo.Transaction,
    name: str,
):
    corpus: py2neo.Node = get_corpus_node_by_name(tx, name)
    tx.delete(corpus)
    tx.graph.commit(tx)
