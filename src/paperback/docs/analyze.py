import py2neo
from fastapi import HTTPException, status

from paperback.docs import schemas


def analyze_predicates(
    tx: py2neo.Transaction,
    docs_and_corpuses: list[schemas.DocsAndCorpuses] | None,
    argument: str,
    predicate: str,
    role: str,
    return_context: bool,
):
    raise HTTPException(
        status_code=status.HTTP_501_NOT_IMPLEMENTED, detail="Not implemented"
    )


def available_statistics():
    raise HTTPException(
        status_code=status.HTTP_501_NOT_IMPLEMENTED, detail="Not implemented"
    )


def analyze_stats(
    tx: py2neo.Transaction,
    docs_and_corpuses: list[schemas.DocsAndCorpuses] | None,
    statistics: list[str],
    analyze_sub_entities: bool,
):
    raise HTTPException(
        status_code=status.HTTP_501_NOT_IMPLEMENTED, detail="Not implemented"
    )
