import uuid
from pathlib import Path
from typing import Callable, TypedDict

import ecdsa
from authlib.jose import errors as jwt_errors
from authlib.jose import jwt
from fastapi import Depends, Header, HTTPException, status

from paperback.auth import schemas
from paperback.auth.crud import get_token
from paperback.auth.database import get_session
from paperback.auth.logging import logger
from paperback.auth.settings import AuthSettings, get_auth_settings

claim_option: dict[str, dict[str, bool | list[str]]] = {
    "iss": {
        "essential": True,
        "values": ["paperback"],
    },
    "sub": {
        "essential": True,
    },
    "exp": {
        "essential": True,
    },
    "jti": {
        "essential": True,
    },
}


def generate_keys(curve: str) -> tuple[bytes, bytes]:
    match curve:
        case "secp521r1":
            logger.debug("creating secp521r1 keys")
            sk: ecdsa.SigningKey = ecdsa.SigningKey.generate(curve=ecdsa.NIST521p)
            vk: ecdsa.VerifyingKey = sk.verifying_key
            sk_bytes: bytes = bytes(sk.to_pem())
            vk_bytes: bytes = bytes(vk.to_pem())
            return sk_bytes, vk_bytes
        case _:
            logger.error("can't find specified curve")
            raise KeyError("can't find specified curve")


def read_keys(
    curve: str, private_key_file: Path, public_key_file: Path
) -> tuple[bytes, bytes]:
    match curve:
        case "secp521r1":
            logger.debug("reading secp521r1 keys")
            sk: ecdsa.SigningKey = ecdsa.SigningKey.from_pem(
                private_key_file.read_text()
            )
            vk: ecdsa.VerifyingKey = sk.verifying_key
            sk_bytes: bytes = bytes(sk.to_pem())
            vk_bytes: bytes = bytes(vk.to_pem())
            return sk_bytes, vk_bytes
        case _:
            logger.error("can't read specified curve")
            raise KeyError("can't read specified curve")


class JWTKeys(TypedDict):
    private_key_file: Path
    public_key_file: Path
    private_key: bytes
    public_key: bytes


keys: JWTKeys | None = None


def get_jwt_keys(settings: AuthSettings = Depends(get_auth_settings)) -> JWTKeys:
    """
    FastAPI dependency
    """
    logger.debug("getting jwt keys")
    global keys

    if keys is None:
        logger.debug("processing jwt keys")
        private_key_file, public_key_file = (
            settings.storage_path / "private.pem",
            settings.storage_path / "public.pem",
        )
        private_key: bytes
        public_key: bytes

        if settings.recreate_keys:
            logger.debug("option for recreating keys is enabled")
            if public_key_file.exists():
                logger.warning("public key exist, saving it")
                bak_public_key_file = Path(str(private_key_file) + ".bak")
                private_key_file.rename(bak_public_key_file)
                public_key_file.touch()
            if private_key_file.exists():
                logger.warning("private key exist, saving it")
                bak_private_key_file = Path(str(private_key_file) + ".bak")
                private_key_file.rename(bak_private_key_file)
                private_key_file.touch()
            if not (public_key_file.exists() and private_key_file.exists()):
                logger.debug("no keys found")
                public_key_file.touch()
                private_key_file.touch()
            logger.debug("generating new keys")
            private_key, public_key = generate_keys(settings.curve)
            logger.debug("saving new keys")
            private_key_file.write_bytes(private_key)
            public_key_file.write_bytes(public_key)
        else:
            if public_key_file.exists() and private_key_file.exists():
                logger.debug("both keys are present")
            else:
                logger.error("one of the keys if missing")
                # TODO: better exception
                raise Exception("one of the keys if missing")
            private_key, public_key = read_keys(
                settings.curve, private_key_file, public_key_file
            )
        keys = JWTKeys(
            private_key_file=private_key_file,
            public_key_file=public_key_file,
            private_key=private_key,
            public_key=public_key,
        )
    else:
        logger.debug("using cached jwt keys")
    return keys


def get_decode_token(
    session=Depends(get_session), jwt_keys: JWTKeys = Depends(get_jwt_keys)
) -> Callable[[str], schemas.Token]:
    """
    FastAPI dependency
    """

    def _decode_token(token: str) -> schemas.Token:
        try:
            claims = jwt.decode(
                token, jwt_keys["public_key"], claims_options=claim_option
            )
            claims.validate()
        except jwt_errors.DecodeError as exception:
            logger.error("can't verify token %s", token)
            logger.error(exception)
            raise HTTPException(
                status_code=status.HTTP_403_FORBIDDEN,
                detail="Can't verify token",
            )
        except jwt_errors.BadSignatureError as exception:
            logger.error(f"can't verify tokens ({token}) signature")
            logger.error(exception)
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Can't verify tokens signature",
            )

        token_uuid = uuid.UUID(claims["jti"])
        token = get_token(session, token_uuid)
        return token

    return _decode_token


def get_token_with_level_of_access(
    greater_or_equal: int | None = None,
    one_of: list[int] | None = None,
):
    """
    FastAPI dependency


    Note
    ----
    must be called with exactly one parameter
    """
    # exactly one must be set
    if (greater_or_equal is not None) and (one_of is not None):
        raise ValueError(
            "greater_or_equal and one_of are provided, can accept only one"
        )
    elif greater_or_equal is None and ((one_of is None) or (len(one_of) == 0)):
        raise ValueError("either greater_or_equal or one_of should be provided")

    def _get_token_with_level_of_access(
        x_authentication: str = Header(...),
        decode_token=Depends(get_decode_token),
    ) -> schemas.Token:
        token: str = x_authentication.removeprefix("Bearer: ")

        token: schemas.Token = decode_token(token)
        user = token.user

        if greater_or_equal is not None:
            if user.level_of_access < greater_or_equal:
                raise HTTPException(
                    status_code=status.HTTP_401_UNAUTHORIZED,
                    detail="Users level_of_access "
                    f"({user.level_of_access}) is lower then "
                    f"required ({greater_or_equal})",
                )
        elif (one_of is not None) and (len(one_of) > 0):
            if user.level_of_access not in one_of:
                raise HTTPException(
                    status_code=status.HTTP_401_UNAUTHORIZED,
                    detail="Users level_of_access "
                    f"({user.level_of_access}) is not in "
                    f"required list ({one_of})",
                )

        return token

    return _get_token_with_level_of_access
