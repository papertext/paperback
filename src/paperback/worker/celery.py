from celery import Celery

from paperback.worker.settings import get_worker_settings

settings = get_worker_settings()

celery = Celery(
    "paperback",
    broker=settings.amqp_url,
    backend=settings.redis_url,
    include=["paperback.worker.tasks"],
)
