from uuid import uuid4

import py2neo
from fastapi import HTTPException, status
from pydantic import parse_obj_as

from paperback.docs import schemas
from paperback.docs.parsers import Parser, ParserEnum, ParserResult
from paperback.worker.tasks import add_document


def create_doc(
    doc: schemas.DocCreate,
    parser_id: str,
) -> str:
    res = add_document.delay(doc.json(ensure_ascii=False), parser_id)
    return res.id


def get_docs(
    tx: py2neo.Transaction,
    tags: list[str] | None = None,
) -> list[schemas.Doc]:
    if tags is None or len(tags) == 0:
        docs: list[py2neo.Node] = tx.graph.nodes.match("Document")
    else:
        docs: list[dict] = tx.run(
            """
        MATCH (d:Document)
        WHERE apoc.coll.contains([tag in $tags| apoc.coll.contains(d.tags, tag)], true)
        return d.name as name, d.doc_uuid as doc_uuid, d.text as text, d.tags as tags;
        """,
            tags=tags,
        ).data()
    return parse_obj_as(list[schemas.Doc], [d for d in docs])


def get_doc_node_by_name(
    tx: py2neo.Transaction,
    name: str,
) -> py2neo.Node:
    doc: py2neo.Node | None = tx.graph.nodes.match("Document", name=name).first()
    if doc is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Document with specified name wasn't found",
        )
    else:
        return doc


def get_doc_by_name(
    tx: py2neo.Transaction,
    name: str,
) -> schemas.Doc:
    doc: py2neo.Node = get_doc_node_by_name(tx, name)
    return parse_obj_as(schemas.Doc, doc)


def add_tag_to_doc_by_name(
    tx: py2neo.Transaction,
    name: str,
    tag: str,
) -> schemas.Doc:
    doc: py2neo.Node = get_doc_node_by_name(tx, name)
    if tag in doc["tags"]:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="Document already has specified doc",
        )
    doc["tags"] += tag
    tx.push(doc)
    tx.graph.commit(tx)
    return parse_obj_as(schemas.Doc, doc)


def delete_tag_from_doc_by_name(
    tx: py2neo.Transaction,
    name: str,
    tag: str,
) -> schemas.Doc:
    doc: py2neo.Node = get_doc_node_by_name(tx, name)
    if tag not in doc["tags"]:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="Document doesn't have specified tag",
        )
    doc["tags"] = list(doc["tags"]).remove(tag)
    tx.push(doc)
    tx.graph.commit(tx)
    return parse_obj_as(schemas.Doc, doc)


def delete_doc_by_name(
    tx: py2neo.Transaction,
    name: str,
):
    doc: py2neo.Node = get_doc_node_by_name(tx, name)
    tx.delete(doc)
    tx.graph.commit(tx)
