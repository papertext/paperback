PaperBack
=========

[//]: # ([![Documentation Status][docs_badge]][docs_link])
[//]: # (TODO: add container registry link)

[![Checked with mypy][mypy_badge]](http://mypy-lang.org/)
[![Code style: black][black_badge]](https://github.com/psf/black)
[![Imports: isort][isort_badge]](https://pycqa.github.io/isort/)

[![License][MIT_license_badge]][license_link]

Status
------

[![pipeline status](https://gitlab.com/papertext/paperback/badges/dev/pipeline.svg)](https://gitlab.com/papertext/paperback/-/commits/dev)


Features
--------
- Uses modern async typed python framework [fastapi](https://fastapi.tiangolo.com/)
- Manages package in `pyproject.toml` with [poetry](https://python-poetry.org/)

Development
-----------
This project contains devcontainer specification which allows it to run in container. Read more [here](https://code.visualstudio.com/docs/remote/containers).

Note: devcontainer relies on docker compose and project currently only tested to work with compose v2. Read about compose v2 [here](https://docs.docker.com/compose/#compose-v2-and-the-new-docker-compose-command).

To start development version simpy run `docker compose up`. Docker will mount source folder, start all required services and run uvicorn in development mode, which allows you to edit code and see it update in real time. 

Contribute
----------
- [Source Code](https://github.com/PaperText/paperback)
- [Issue Tracker](https://github.com/PaperText/paperback/issues)

Support
-------
If you are having issues, please let us know through [issue tracker](https://github.com/PaperText/paperback/issues)

License
-------
The project is licensed under the MIT license.

<!-- links -->
[docs_badge]: https://readthedocs.org/projects/paperback/badge/?version=latest&style=flat-square
[docs_link]: https://paperback.readthedocs.io/en/latest/?badge=latest

[container_badge]: <>
[container_link]: <>

[mypy_badge]: https://img.shields.io/badge/mypy-checked-2a6db2?style=flat-square
[black_badge]: https://img.shields.io/badge/code%20style-black-000000.svg?style=flat-square
[isort_badge]: https://img.shields.io/badge/imports-isort-1674b1?style=flat-square&labelColor=ef8336

[MIT_license_badge]: https://img.shields.io/badge/License-MIT-yellow.svg?style=flat-square
[license_link]: https://gitlab.com/PaperText/paperback/blob/master/LICENSE
