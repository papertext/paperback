import logging
import time
import uuid
from typing import Callable

from fastapi import FastAPI, Request
from fastapi.middleware.cors import CORSMiddleware

from paperback import __version__
from paperback.auth.router import auth_router
from paperback.docs.router import docs_router
from paperback.logger import setup_logging
from paperback.settings import get_settings
from paperback.util import get_response_class

# global settings
settings = get_settings()

# setup logging
setup_logging(settings)

# get paperback logger
logger = logging.getLogger("paperback")

# create app
logger.info("initializing PaperBack app")

app = FastAPI(
    title="PaperText backend [Paperback]",
    # TODO: replace with description from package
    description="Backend API for PaperText",
    version=__version__,
    openapi_tags=[
        {"name": "root", "description": "administrative functions"},
        # auth
        {"name": "auth", "description": "authorization"},
        {"name": "token", "description": "token manipulation"},
        {"name": "user", "description": "users manipulation"},
        # {"name": "organisation", "description": "organisation manipulation"},
        # {"name": "invite", "description": "invite codes manipulation"},
        # docs
        {"name": "docs", "description": "document manipulation"},
        {"name": "corpus", "description": "corpus manipulation"},
        {"name": "dict", "description": "dictionaries manipulation"},
        {"name": "analyzer", "description": "analyzer usage"},
        # ] + [
        #     {
        #         "name": f"access_level_{i}",
        #         "description": f"paths that require level {i} access",
        #     }
        #     for i in range(4)
    ],
    docs_url="/spec",
    redoc_url="/re_spec",
    default_response_class=get_response_class(),
)

# adding middleware
logger.info("adding middleware")

logger.debug("adding CORS middleware")
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

logger.debug("adding process time middleware")


@app.middleware("http")
async def add_process_time_header(request: Request, call_next: Callable):
    start_time: float = time.time()
    response = await call_next(request)
    process_time: float = time.time() - start_time
    response.headers["X-Process-Time"] = f"{process_time:.2f} seconds"
    return response


# default routes
logger.info("adding default routes")

logger.debug("adding /info path")


@app.get("/info", tags=["root"])
def stats():
    """basic app info"""
    return {
        "version": __version__,
        "is_uuid_safe": uuid.uuid4().is_safe.name,
    }


# adding default routers
logger.info("adding default routers")

logger.debug("adding auth routers")
app.include_router(auth_router)

logger.debug("adding docs routers")
app.include_router(docs_router)
