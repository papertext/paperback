from __future__ import annotations

from enum import Enum
from typing import Any, Literal, TypedDict

from celery.states import ALL_STATES
from pydantic import BaseModel, EmailStr, Field, UUID4

from paperback.docs.parsers import DEFAULT_PARSER, ParserEnum

# task
# ----


class TaskState(BaseModel):
    state: str
    # ALL_STATES
    info: str | Any


# doc
# ---


class DocBase(BaseModel):
    name: str
    text: str

    tags: list[str] = []


class DocCreate(DocBase):
    pass


class DocOut(DocBase):
    pass


class Doc(DocBase):
    doc_uuid: UUID4

    class Config:
        orm_mode = True


# corp
# ----


class CorpusBase(BaseModel):
    name: str


class CorpusCreate(CorpusBase):
    pass


class CorpusOut(CorpusBase):
    includes_docs: list[str] = []
    includes_corps: list[str] = []


class Corpus(CorpusBase):
    corpus_uuid: UUID4

    includes_docs: list[str] = []
    includes_corps: list[str] = []

    class Config:
        orm_mode = True


# dict
# ----


class DictionaryBase(BaseModel):
    name: str
    words: list[str] = []


class DictionaryCreate(DictionaryBase):
    pass


class DictionaryOut(DictionaryBase):
    pass


class Dictionary(DictionaryBase):
    dict_uuid: UUID4

    class Config:
        orm_mode = True


# analyzer
# --------


class DocOrCorp(str, Enum):
    doc = "doc"
    corp = "corp"


class DocsAndCorpuses(BaseModel):
    type: DocOrCorp
    name: str
    analyzed_by: ParserEnum = DEFAULT_PARSER


class AnalyzePredicates(BaseModel):
    docs_and_corpuses: list[DocsAndCorpuses] | None = Field(
        None, description="list of documents and corpuses to analyze"
    )
    argument: str | None = None
    predicate: str | None = None
    role: str | None = None


class AnalyzeStats(BaseModel):
    docs_and_corpuses: list[DocsAndCorpuses] | None = Field(
        None, description="list of documents and corpuses to analyze"
    )
    statistics: list[str]
