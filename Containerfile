ARG BUILD_FROM=docker.io/library/python:3.10-slim
FROM ${BUILD_FROM}

EXPOSE 7878

# no buffer, straight to logs
ENV PYTHONUNBUFFERED=1
# don't create __pycache__ folders, usefull for development with compose
# ENV PYTHONDONTWRITEBYTECODE=1

LABEL version="0.1"
LABEL description="containerized version of paperback"
LABEL org.opencontainers.image.authors="Danil Kireev <kireev@isa.ru>"

# install deps with apt
ENV DEBIAN_FRONTEND="noninteractive"
RUN apt-get update && \
    apt-get install --no-install-recommends -y \
        git \
        build-essential \
        libmpc-dev && \
    apt-get clean
ENV DEBIAN_FRONTEND=

# upgrade pip and setuptools
RUN python -m pip install --upgrade pip setuptools

# install poetry
RUN pip install poetry
# add executable to path
ENV PATH /root/.local/bin:$PATH
# disable creating venv
ENV POETRY_VIRTUALENVS_CREATE = 0

# create folders with cache and source code
WORKDIR /paperback
RUN mkdir /.papertext
RUN chmod a+rw /.papertext/

# minimal source code for package installation
COPY pyproject.toml   ./
COPY poetry.lock      ./
COPY LICENSE          ./
COPY README.md        ./
COPY ./src/paperback/ ./src/paperback/

RUN poetry install -E all

CMD uvicorn --host 0.0.0.0 --port 7878 --log-level info --use-colors paperback.app:app
