from functools import lru_cache

from pydantic import AmqpDsn, AnyUrl, BaseSettings, RedisDsn


class WorkerSettings(BaseSettings):
    amqp_url: AmqpDsn
    redis_url: RedisDsn
    neo4j_url: AnyUrl | None
    parsers_titanis_host: str | None
    parsers_pyexling_host: str | None
    parsers_pyexling_service: str | None

    class Config:
        env_prefix = "worker__"


class StrictWorkerSettings(WorkerSettings):
    neo4j_url: AnyUrl
    parsers_titanis_host: str
    parsers_pyexling_host: str
    parsers_pyexling_service: str

    class Config:
        env_prefix = "worker__"


@lru_cache()
def get_worker_settings() -> WorkerSettings:
    return WorkerSettings()


@lru_cache()
def get_worker_settings_in_worker() -> StrictWorkerSettings:
    return StrictWorkerSettings()
