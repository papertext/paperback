import uvicorn

if __name__ == "__main__":
    uvicorn.run(
        "paperback.app:app",
        host="0.0.0.0",
        port=7878,
        log_level="info",
        use_colors=True,
    )
