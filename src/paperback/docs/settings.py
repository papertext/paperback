from functools import lru_cache
from typing import Any, cast

from pydantic import AmqpDsn, BaseSettings, RedisDsn


class DocsSettings(BaseSettings):
    neo4j_scheme: str = "bolt"
    neo4j_user: str = "neo4j"
    neo4j_pass: str = "password"
    neo4j_host: str = "localhost"
    neo4j_port: str = "7687"

    parsers_pyexling_host: str
    parsers_pyexling_service: str

    parsers_titanis_host: str

    class Config:
        env_prefix = "docs__"


@lru_cache()
def get_docs_settings() -> DocsSettings:
    """
    FastAPI dependency
    """
    return DocsSettings()
