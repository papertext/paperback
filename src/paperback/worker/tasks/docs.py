from typing import Annotated, cast
from uuid import uuid4

import py2neo
from celery import states
from celery.exceptions import Ignore
from celery.utils.log import get_task_logger
from fastapi import HTTPException, status

from paperback.docs import schemas
from paperback.docs.parsers import Parser, ParserEnum, ParserEnum2Wrapper, ParserResult
from paperback.docs.parsers.implementation import PyExLingWrapper, TitanisWrapper
from paperback.worker.celery import celery
from paperback.worker.settings import get_worker_settings_in_worker

logger = get_task_logger(__name__)


@celery.task(bind=True)
def add_document(
    self,
    doc_as_json: Annotated[str, schemas.DocCreate],
    parser_id: ParserEnum,
):
    logger.info(
        f"""starting `add_document` task with id `{self.request.id}` with followiing arguments:
        doc={doc_as_json},
        parser_id={parser_id},
        """
    )
    self.update_state(state=states.STARTED)

    settings = get_worker_settings_in_worker()
    logger.debug(f"settings in task: {settings.json(ensure_ascii=False)}")

    # convert to pydantic model
    doc = schemas.DocCreate.parse_raw(doc_as_json)

    # connecting to parser
    parser_enum_id = ParserEnum(parser_id)
    parser_wrapper = ParserEnum2Wrapper[parser_enum_id]
    parser: Parser

    # TODO: replace with match?
    if parser_enum_id is ParserEnum.pyexling:
        parser_wrapper = cast(type[PyExLingWrapper], parser_wrapper)
        parser = parser_wrapper(
            settings.parsers_pyexling_host,
            settings.parsers_pyexling_service,
            settings.parsers_titanis_host,
        )
    elif parser_enum_id is ParserEnum.titanis_open:
        parser_wrapper = cast(type[TitanisWrapper], parser_wrapper)
        parser = parser_wrapper(
            settings.parsers_titanis_host,
        )

    # conntecting to graph
    graph = py2neo.Graph(settings.neo4j_url)
    tx = graph.begin()

    doc_node = py2neo.Node(
        "Document",
        name=doc.name,
    )

    if tx.graph.nodes.match(*doc_node.labels, **dict(doc_node)).exists():
        reason = f"Document with name `{doc.name}` already exists"

        logger.exception(reason)

        self.update_state(
            state="DOCUMENT-ALREADY-EXISTS",
            meta={"custom": reason},
        )

        raise Ignore()

    try:
        doc_node["text"] = doc.text
        doc_node["tags"] = doc.tags
        doc_node["doc_uuid"] = str(uuid4())
        tx.create(doc_node)

        info = f"processing text with parser `{parser_id}`"
        logger.info(info)
        self.update_state(
            state=states.PENDING,
            meta=info,
        )

        # add node with parser results
        parser_res_node = py2neo.Node("ParserResult", parser_id=parser_enum_id.value)
        tx.create(parser_res_node)
        tx.create(py2neo.Relationship(doc_node, "analyzed", parser_res_node))

        parser_result: ParserResult = parser(doc.text, parser_res_node)

        info = f"saving processing result"
        logger.info(info)
        self.update_state(
            state=states.PENDING,
            meta=info,
        )

        for node in parser_result["nodes"]:
            tx.create(node)

        for relationship in parser_result["relationships"]:
            tx.create(relationship)

        for command in parser_result["commands_to_run"]:
            tx.run(command)

        tx.graph.commit(tx)
    except Exception as exc:
        logger.exception("something went wrong")

        self.update_state(state=states.FAILURE)
        tx.graph.rollback(tx)

        self.retry(exc=exc)
