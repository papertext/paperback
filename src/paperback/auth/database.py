import asyncio
from typing import Iterator

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import Session, sessionmaker

from paperback.auth.settings import get_auth_settings

settings = get_auth_settings()

engine = create_engine(
    f"postgresql+psycopg2://{settings.psql_user}:{settings.psql_pass}@{settings.psql_host}:{settings.psql_port}/{settings.psql_name}",
    future=True,
)
local_session = sessionmaker(
    engine,
    autocommit=False,
    autoflush=False,
    expire_on_commit=False,
    future=True,
)

Base = declarative_base()


def get_session() -> Iterator[Session]:
    """
    FastAPI dependency
    """
    session = local_session()
    try:
        yield session
    finally:
        session.close()
