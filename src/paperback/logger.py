import logging
from logging.handlers import RotatingFileHandler
from pathlib import Path

from uvicorn.logging import ColourizedFormatter

from paperback.settings import AppSettings


def setup_logging(settings: AppSettings):
    config_dir = Path(settings.config_dir).resolve()

    # setup paperback logger
    paperback_logger = logging.getLogger("paperback")
    paperback_logger.setLevel(settings.log_level)

    logs_dir = config_dir / "logs"
    if logs_dir.exists() and logs_dir.is_dir():
        paperback_logger.debug("found logging folder")
    else:
        paperback_logger.debug("can't find logging folder, creating it")
        logs_dir.mkdir(parents=True)
        paperback_logger.debug("created logging folder")

    # file handler
    file_handler = RotatingFileHandler(
        logs_dir / "root.log",
        maxBytes=1024**3,
        backupCount=10,
    )
    file_handler.setFormatter(
        logging.Formatter(
            "{levelname:<8} in {name:<18} at {asctime:<16}: {message}",
            "%Y-%m-%d %H:%M:%S",
            style="{",
        )
    )
    paperback_logger.addHandler(file_handler)

    # console handler
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(
        ColourizedFormatter(
            "{levelprefix:<8} @ {name:<18} : {message}",
            "%Y-%m-%d %H:%M:%S",
            style="{",
            use_colors=True,
        )
    )
    paperback_logger.addHandler(console_handler)
